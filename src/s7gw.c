#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <windows.h>
#include "../inc/snap7.h"
#include "../inc/udp2mat.h"

#define malloc_w(m) HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS|HEAP_ZERO_MEMORY, (m))
#define free_w(m)  HeapFree(GetProcessHeap(),0,(LPVOID) (m))

PCHAR s7_ip=NULL,udpclient_ip=NULL,WorkArea=NULL;
PBYTE InputCoil=NULL,OutputCoil=NULL;
UINT16 Rack,Slot,UdpServ_port,UdpClient_port,InpDBnum,InpDBoffset,InpDBlen,OutDBnum,OutDBoffset,OutDBlen;
DWORD tout;
int GetInitParam(PCHAR mask)
{
    WIN32_FIND_DATA findData;
    HANDLE f;
    if((f=FindFirstFile(mask,&findData))==INVALID_HANDLE_VALUE)
        return -1;
    //------------------------------------------------------
    if(WorkArea==NULL)
        WorkArea=malloc_w(0x1000);
    GetCurrentDirectory(0x200,WorkArea);
    lstrcat((LPSTR)WorkArea,"\\");
    lstrcat((LPSTR)WorkArea,(LPCSTR)findData.cFileName);
    FindClose(f);
    //-----------------------------------------------------
    if(s7_ip==NULL)
        s7_ip=malloc_w(0x10);
    GetPrivateProfileString("s7config","ip","127.0.0.1",s7_ip,15,(LPCSTR)WorkArea);
    Slot=GetPrivateProfileInt("s7config","slot",0,(LPCSTR)WorkArea);
    Rack=GetPrivateProfileInt("s7config","rack",0,(LPCSTR)WorkArea);
    //-------------------------------------------------------
    UdpServ_port=GetPrivateProfileInt("udpDiscretInput","port",2301,(LPCSTR)WorkArea);
    tout=GetPrivateProfileInt("udpDiscretInput","timeout",1000,(LPCSTR)WorkArea);
    //-------------------------------------------------------
    if(udpclient_ip==NULL)
        udpclient_ip=malloc_w(0x10);
    GetPrivateProfileString("udpDiscretOutput","ip","127.0.0.1",udpclient_ip,15,(LPCSTR)WorkArea);
    UdpClient_port=GetPrivateProfileInt("udpDiscretOutput","port",2300,(LPCSTR)WorkArea);
    //-------------------------------------------------------
    InpDBnum=GetPrivateProfileInt("s7DiscretInput","db",1,(LPCSTR)WorkArea);
    InpDBoffset=GetPrivateProfileInt("s7DiscretInput","offset",0,(LPCSTR)WorkArea);
    InpDBlen=GetPrivateProfileInt("s7DiscretInput","len",0,(LPCSTR)WorkArea);
    if(InputCoil==NULL)
        InputCoil=malloc_w((abs(InpDBlen)<<1)+2);
    //-------------------------------------------------------
    OutDBnum=GetPrivateProfileInt("s7DiscretOutput","db",2,(LPCSTR)WorkArea);
    OutDBoffset=GetPrivateProfileInt("s7DiscretOutput","offset",0,(LPCSTR)WorkArea);
    OutDBlen=GetPrivateProfileInt("s7DiscretOutput","len",0,(LPCSTR)WorkArea);
    if(OutputCoil==NULL)
        OutputCoil=malloc_w((abs(OutDBlen)<<1)+2);

    return 1;
}

int InitConnectUDP(void)
{
    int rc;
    rc = UDP_Init();
    rc *=UDP_OpenSend();
    rc *=UDP_OpenReceive();
    return rc;
}

int cmp2buf(PVOID _mem1, PVOID _mem2, int len)
{
    int rc;

    if((rc=memcmp((const void*)_mem1,(const void *) _mem2,len)))
        memcpy((void *)_mem2,(void *) _mem1,len);

    return rc;
}

int InitDB(S7Object Cln,int InDBnum, int InDBoff,int InDBlen,PVOID InDBCoil)
{
    int res=-1;
    ZeroMemory(InDBCoil,(InDBlen<<1));
    res = Cli_DBWrite(Cln, InDBnum, InDBoff, InDBlen,(void *) InDBCoil);
    return res;
}

int main()
{
    S7Object Client=0;
    int res,TotalSend=0,TotalRecv=0,i;
    DWORD rc,LocTime,DiffTime;
    int LostConnect=0;

    if(GetInitParam("*.ini")<0)
        return -1;

    Client=Cli_Create();
    res = Cli_ConnectTo(Client,s7_ip,Rack,Slot);
    Cli_ErrorText(res, WorkArea, 1024);
    printf("Press any key for finish...\n");
    res = InitConnectUDP();
    res = InitDB(Client, OutDBnum, OutDBoffset, OutDBlen,(void *) OutputCoil);
    res = InitDB(Client, InpDBnum, InpDBoffset, InpDBlen,(void *) InputCoil);
    while(!kbhit())
    {
        res = Cli_DBRead(Client, OutDBnum, OutDBoffset, OutDBlen,(void *) OutputCoil);
        Cli_ErrorText(res, WorkArea, 1024);
        if(!res)
            if(cmp2buf((PVOID) OutputCoil, (PVOID) (OutputCoil+OutDBlen), OutDBlen))
            {
                TotalSend=UDP_Send(udpclient_ip,UdpClient_port,(PVOID) OutputCoil, OutDBlen);
                printf("%lu:\tDB%d.DBB%d BYTE %d\t%s\n",GetTickCount(),OutDBnum,OutDBoffset,TotalSend,WorkArea);
                printf("Read:\t");
                for(i=0; i<OutDBlen; i++)
                    printf("[%03d] ",OutputCoil[i]);
                printf("\n");
            }

        TotalRecv=UDP_Receive(UdpServ_port,InpDBlen,(PVOID)InputCoil);
        if(TotalRecv > 0)
        {
            if(cmp2buf((PVOID) InputCoil, (PVOID) (InputCoil+InpDBlen), InpDBlen))
            {
                res = Cli_DBWrite(Client, InpDBnum, InpDBoffset, InpDBlen,(void *) InputCoil);
                Cli_ErrorText(res, WorkArea, 1024);
                printf("%lu:\tDB%d.DBB%d BYTE %d\t%s\n",GetTickCount(),InpDBnum,InpDBoffset,TotalRecv,WorkArea);
                printf("Write:\t");
                for(i=0; i<InpDBlen; i++)
                    printf("[%03d] ",InputCoil[i]);
                printf("\n");
            }
        }
        if((TotalSend < 0) || (TotalRecv < 0))
        {
            UDP_Close();
            res = InitConnectUDP();
            TotalRecv=TotalSend=0;
        }
        Sleep(tout);
    }

    Cli_Destroy(&Client);
    res = UDP_Close();
    //------------------------------------------------------
    free_w(WorkArea);
    free_w(s7_ip);
    free_w(udpclient_ip);
    free_w(InputCoil);
    free_w(OutputCoil);
    return 0;
}

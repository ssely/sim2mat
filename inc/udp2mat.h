#ifndef _udp2mat_h
#define _udp2mat_h

extern int UDP_Init(void);
extern int UDP_Close(void);
extern int UDP_Send(PCHAR,int,PVOID,int);
extern int UDP_OpenSend(void);
extern int UDP_OpenReceive(void);
extern int UDP_Receive(int,int,PVOID);
#endif //_udp2mat_h